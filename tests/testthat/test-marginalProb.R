test_that("marginalProb", {

  numiter <- 1000000

  expect_equal(
    round(marginalProb(rowA = c(1, 0), deltas = c(0.5, 0), baselineExt = 0.1,
                       alpha = 1, beta = 1, nReps = numiter) / numiter, 2),
    0.55
  )

  expect_equal(
    round(marginalProb(rowA = c(0, 1, 0), deltas = c(0.1, 0.19), baselineExt = 0.1,
                       alpha = 1, beta = 1, nReps = numiter) / numiter, 2),
    0.27
  )

})
