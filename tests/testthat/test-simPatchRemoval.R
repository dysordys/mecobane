test_that("simPatchRemoval_1", {

  edgeList <- read.csv("../data/test_web.csv")
  speciesInputTable <- read.csv("../data/test_species_input.csv")
  landscapeTable <- read.csv("../data/test_landscape.csv")
  focalSpecies <- "species001"
  alpha <- 1
  beta <- 1
  nReps <- 10000L
  iter <- 5L
  atol <- 1e-10
  toRemove <- 1L
  scenario <- "worst"
  outfile <- "test_community_worst.csv"

  result <- simPatchRemoval(edgeList, speciesInputTable, landscapeTable, focalSpecies,
                            alpha, beta, nReps, iter, atol, toRemove, scenario, outfile)

  expect_equal(result, NULL)

  commTabSeq <- read.csv(outfile)
  expect_equal(
    unique(commTabSeq[c("species", "patchesLeft")])$patchesLeft,
    rep(20:4, each = 4)
  )

  file.remove(outfile)

})


test_that("simPatchRemoval_2", {

  edgeList <- read.csv("../data/test_web.csv")
  speciesInputTable <- read.csv("../data/test_species_input.csv")
  landscapeTable <- read.csv("../data/test_landscape.csv")
  focalSpecies <- "species001"
  alpha <- 1
  beta <- 1
  nReps <- 10000L
  iter <- 5L
  atol <- 1e-10
  toRemove <- 5L
  scenario <- "best"
  outfile <- "test_community_best.csv"

  result <- simPatchRemoval(edgeList, speciesInputTable, landscapeTable, focalSpecies,
                            alpha, beta, nReps, iter, atol, toRemove, scenario, outfile)

  expect_equal(result, NULL)

  commTabSeq <- read.csv(outfile)
  expect_equal(
    unique(commTabSeq[c("species", "patchesLeft")])$patchesLeft,
    rep(c(20, 15, 10, 5, 4, 3, 2), each = 4)
  )

  file.remove(outfile)

})


test_that("simPatchRemoval_invalid", {

  edgeList <- read.csv("../data/test_web.csv")
  speciesInputTable <- read.csv("../data/test_species_input.csv")
  landscapeTable <- read.csv("../data/test_landscape.csv")
  focalSpecies <- "some nonexisting species"
  alpha <- 1
  beta <- 1
  nReps <- 10000L
  iter <- 5L
  atol <- 1e-10
  toRemove <- 5L
  scenario <- "best"
  outfile <- "test_community_best.csv"

  result <- simPatchRemoval(edgeList, speciesInputTable, landscapeTable, focalSpecies,
                            alpha, beta, nReps, iter, atol, toRemove, scenario, outfile)

  expect_equal(result, NULL)

})
