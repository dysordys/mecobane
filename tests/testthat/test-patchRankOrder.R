test_that("patchRankOrder", {

  commTab <- communityTable(read.csv("../data/test_species_input.csv"),
                            read.csv("../data/test_landscape.csv"))
  commTab <- simMetacomm(commTab, read.csv("../data/test_web.csv"))

  order1 <- patchRankOrder(commTab, "species001")
  order2 <- order(speciesTable(commTab, "species001")$patchValue, decreasing = TRUE)

  expect_equal(order1, order2)

})
