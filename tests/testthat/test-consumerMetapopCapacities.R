test_that("consumerMetapopCapacities", {

  lM <- 20
  web <- data.frame(consumer = c("herbivore", "carnivore"),
                    resource = c("plant", "herbivore"))

  commTab <- data.frame(species = c("plant", "herbivore", "carnivore"),
                        lambda = -lM / log(0.5))
  for (i in 2:nrow(commTab)) {
    L <- 1 / commTab$lambda[i - 1]
    commTab$lambda[i] <- lM/(lM * L - log(0.5*(1 - L)))
  }
  commTab["occupancy"] <- 1 - 1 / commTab$lambda
  commTab$lambda <- round(commTab$lambda, 1)
  commTab$occupancy <- round(commTab$occupancy, 1)
  expect_equal(consumerMetapopCapacities(commTab, web), c(14.1, 9.1))


  commTab2 <- communityTable(read.csv("../data/test_species_input.csv"),
                             read.csv("../data/test_landscape.csv"))
  commTab2 <- simMetacomm(commTab2, read.csv("../data/test_web.csv"))
  expect_equal(
    round(consumerMetapopCapacities(commTab2, read.csv("../data/test_web.csv"))),
    c(18, 14)
  )

})
