#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;


//' Calculate local extinction probabilities (deltas) of a focal species
//' in a focal patch.
//'
//' @param rowA One row of the food web's adjacency matrix, corresponding to the
//' focal species.
//' @param deltas Vector of species' extinction probabilities in focal patch.
//' @param baselineExt Baseline extinction probability of focal species in focal patch.
//' @param alpha First parameter of the Beta distribution.
//' @param beta Second parameter of the Beta distribution.
//' @param nReps Number of iterations for the Monte Carlo simulation
//' of the Bayesian network.
//'
//' @return Number of times the focal species in the focal patch went extinct
//' (out of \code{nReps} iterations).
//' @export
//'
//' @useDynLib mecobane, .registration = TRUE
//' @importFrom Rcpp evalCpp
//'
// [[Rcpp::export]]
int marginalProb(NumericVector rowA, NumericVector deltas, double baselineExt,
              double alpha, double beta, int nReps) {
  int S = rowA.size(); // Number of species
  int marginal = 0;
  double frac, pExt, prodsum, tot;
  NumericVector extinct(S), randE(S), randR = Rcpp::runif(nReps);
  for (int rep = 0; rep < nReps; rep++) {
    prodsum = 0.0; // Cumulative sum of product of rowA[i] and extinct[i]
    tot = 0.0; // Sum of rowA's entries (row sum of A[sp,])
    randE = Rcpp::runif(S); // Generate S random numbers, for future use
    for (int i = 0; i < S; i++) { // For each species:
      extinct[i] = (randE[i] < deltas[i] ? 1.0 : 0.0); // Extinct or extant?
      prodsum += rowA[i] * extinct[i]; // Is ith species extinct?
      tot += rowA[i]; // Accumulate sum of entries of rowA
    }
    frac = prodsum / tot; // Fraction of extinct prey
    pExt = baselineExt + (1 - baselineExt) * R::pbeta(frac, alpha, beta, true, false);
    marginal += (randR[rep] < pExt ? 1 : 0); // Extinct if random number < pExt
  }
  return(marginal);
}
