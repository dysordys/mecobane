#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;


//' Numerically calculate equilibrium patch occupancies
//'
//' @param p0 Vector of initial patch occupancies of the focal species.
//' @param M Dispersal matrix of the focal species.
//' @param deltas Vector of extinction probabilities of the focal species
//' across the patches.
//' @param iter Number of iterations (as an integer) for updating the solution,
//' before the improvement in convergence is assessed.
//' @param atol Absolute tolerance: the sum over the patches of the squared
//' differences between the previous solution and the updated one
//' (after \code{iter} iterations) may not be larger than this.
//'
//' @return Vector of equilibrium patch occupancies for the focal species.
//' @export
//'
//' @useDynLib mecobane, .registration = TRUE
//' @importFrom Rcpp evalCpp
//'
// [[Rcpp::export]]
NumericVector itersol(NumericVector p0, NumericMatrix M, NumericVector deltas,
                      int iter = 5, double atol = 1e-10) {
  int N = p0.size(); // Number of patches
  double error = 2.0 * atol; // For testing convergence of solution
  NumericVector pPrev(N), pNext(N), E(N), C(N);
  for (int j = 0; j < N; j++) { // Initialization
    pPrev[j] = p0[j]; // The current solution; initially just p0
    pNext[j] = p0[j]; // Solution after iterations; initialized to p0 as well
    E[j] = log(1.0 / (1.0 - deltas[j])); // Extinction rates
  }
  while (error > atol) { // If solution hasn't converged yet:
    for (int i = 0; i < iter; i++) { // For a few iterations:
      for (int k = 0; k < N; k++) { // Calculate colonization rate for patch k:
        C[k] = 0.0; // Initialize kth entry of colonization
        for (int l = 0; l < N; l++) C[k] = C[k] + M(k,l) * pNext[l]; // Colonization
        pNext[k] = C[k] / (C[k] + E[k]); // Iteration map
      }
    }
    error = sum((pPrev - pNext) * (pPrev - pNext)); // Degree of convergence
    for (int j = 0; j < N; j++) pPrev[j] = pNext[j]; // Update current estimate of p
  }
  return(pPrev);
}
