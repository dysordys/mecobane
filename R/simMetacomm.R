#' Obtain metacommunity equilibrium state
#'
#' @inheritParams metapopCapacities
#' @inheritParams adjacencyMatrix
#' @param alpha First parameter of the Beta distribution used in the parameterization
#' of the conditional extinction probabilities in the Bayesian network.
#' @param beta Second parameter of the Beta distribution used in the parameterization
#' of the conditional extinction probabilities in the Bayesian network.
#' @param nReps Number of iterations for the Monte Carlo simulation
#' of the Bayesian network.
#' @param iter Number of iterations (as an integer) for updating the solution
#' before the improvement in convergence is assessed.
#' @param atol Absolute tolerance: the sum over the patches of the squared
#' differences between the previous solution and the updated one
#' (after \code{iter} iterations) may not be larger than this.
#'
#' @return A community table updated with the equilibrium marginal extinction
#' probabilities, patch occupancies, metapopulation capacities, and patch values.
#'
#' @export
#'
#' @importFrom tidyr crossing
#' @importFrom dplyr left_join
#' @importFrom tibble tibble
#' @importFrom tibble as_tibble
#'
#' @examples
#' commTab <- communityTable(
#'   data.frame(species = c("plant", "herbivore"),
#'              pi = 0.1,
#'              xi = 0.1,
#'              kernel = c("Exponential", "Gaussian")),
#'   data.frame(dim1 = c(0.2, 0.8, 0.1),
#'              dim2 = c(0.1, 0.8, 0.2))
#' )
#' commTab["delta"] <- commTab$pi
#' edgeList = data.frame(consumer = "herbivore", resource = "plant")
#'
#' simMetacomm(commTab, edgeList)
#'
simMetacomm <- function(commTab, edgeList, alpha = 1, beta = 1, nReps = 10000L,
                        iter = 5L, atol = 1e-10) {
  adjMat <- adjacencyMatrix(edgeList)
  commTab$delta <- commTab$pi
  commTab$occupancy <- 0.5
  # Rearrange rows in table so species order corresponds to the sorting in adjMat:
  commTab <- left_join(tibble(species = rownames(adjMat)), commTab, by = "species")
  for (sp in rownames(adjMat)) {
    marginal <- bayesExtinctionProb(commTab, sp, adjMat, alpha, beta, nReps)
    commTab$delta[commTab$species == sp] <- marginal
    M <- dispMatrixFromTab(commTab, species = sp)
    commTab$occupancy[commTab$species == sp] <- itersol(
      commTab$occupancy[commTab$species == sp], M, commTab$delta[commTab$species == sp],
      iter, atol)
    spectrum <- jacSpectrum(commTab, species = sp)
    commTab$lambda[commTab$species == sp] <- spectrum$metapopCapacity
    commTab$patchValue[commTab$species == sp] <- diag(spectrum$sensitivityMatrix)
  }
  as_tibble(commTab)
}
