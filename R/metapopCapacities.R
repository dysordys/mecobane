#' Metapopulation capacity of each species
#'
#' @param commTab Species and landscape data, as produced by the function
#' \code{communityTable} or \code{simMetacomm}.
#'
#' @return A tibble with two columns: species name and metapopulation capacity.
#'
#' @export
#'
#' @examples
#' commTab <- communityTable(
#'   speciesInputTable = data.frame(
#'     species = c("plant", "herbivore"),
#'     pi = 0.1,
#'     xi = 0.2,
#'     kernel = "Exponential"
#'   ),
#'   landscapeTable = data.frame(
#'     dim1 = c(0.3, 0.8),
#'     dim2 = c(0.8, 0.3)
#'   )
#' )
#' commTab["lambda"] <- c(1.1, 1.1, 2, 2)
#'
#' metapopCapacities(commTab)
#'
metapopCapacities <- function(commTab) {
  unique(commTab[c("species", "lambda")])
}
